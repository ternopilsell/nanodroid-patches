# NanoDroid patches

## GNU Bash

NanoDroid uses the (at build time) latest Debian GNU/Linux source tarball for Bash, so it includes all patches from Debian. See

* https://packages.debian.org/source/unstable/bash

for the source tarball.

### Building GNU Bash

* Apply bash-xxx.diff where xxx is the version numer of GNU Bash
* Assuming your build environment is up-to-date, run for the x86 build
  * config:		`CFLAGS="-m32" ./configure --host=i686-pc-linux-gnu --enable-static-link --without-bash-malloc`
  * build:		`make`
  * strip binary:	`x86_64-linux-gnu-strip -o bash-stripped.x86 -s bash`
     * `bash-stripped.x86` is your final build you can use
* now for the arm build
  * config:		`./configure --host=arm-linux-gnueabi --enable-static-link --without-bash-malloc`
  * build:		`make`
  * strip binary:	`arm-linux-gnueabihf-strip -o bash-stripped.arm -s bash`
     * `bash-stripped.arm` is your final build you can use

## MPV-Android

* clone the official mpv-android repo from
  * https://github.com/mpv-android/mpv-android
* apply mpv-nightly.diff to tell gradle to use a different versioning scheme, more suitable for nightly builds
* optionally apply ytdl branch, as in NanoDroid MPV builds, see:
  * https://github.com/mpv-android/mpv-android/pull/58
* follow the official build instructions
  * https://github.com/mpv-android/mpv-android/tree/master/buildscripts#building

## SQLite 3

See the separate Git repository:

* https://gitlab.com/Nanolx/nanodroid-sqlite3-build

## Google Play Store

See the separate Git repository:

* https://gitlab.com/Nanolx/microg-phonesky-iap-support
